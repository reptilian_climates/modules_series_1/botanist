


'''
	pytest
		sh:
			(cd ../fields/gardens && PYTHONPATH="gardens_pip:gardens" ./gardens_pip/bin/pytest --json-report --json-report-summary)
			(cd ../fields/gardens && PYTHONPATH="../gardens_pip:../gardens" ./../gardens_pip/bin/pytest -s)
		
'''

# module_to_check = "latest pip"
module_to_check = "local"

module_paths = [
	'../fields/gardens_pip'
]
if (module_to_check == "local"):
	module_paths.insert (0, '../fields/gardens')
elif (module_to_check == "latest pip"):
	module_paths.insert (0, '../fields/gardens_latest_pip')

